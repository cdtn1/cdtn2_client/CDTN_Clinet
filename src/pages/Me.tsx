import React from "react";
import { Routes, Route } from "react-router-dom";

import { Menu, MyBooking, MyReview, MyInfo, ManagerTourAd } from "../components/User";
export default function Me() {
  return (
    <main className="main">
      <div className="user-view">
        <Menu />
        <div className="user-view__content">
          <Routes>
            <Route path="" element={<MyInfo />} />
            <Route path="bookings" element={<MyBooking />} />
            <Route path="reviews" element={<MyReview />} />
            <Route path="mangertour" element={<ManagerTourAd />} />
          </Routes>
        </div>
      </div>
    </main>
  );
}
