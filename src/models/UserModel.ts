export interface User {
  _id: string;
  name: string;
  accessToken: string | undefined;
  email: string;
  address: string;
  role: string;
  photo: string;
  name_photo: string;
  password: string;
  passwordConfirm?: string;
  passwordChangedAt: Date;
}
