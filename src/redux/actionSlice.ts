import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Booking } from "../models/BookingModel";
import { Review } from "../models/ReviewModel";
interface BookingState {
  bookings: Booking[];
  reviews: Review[];
  loading: boolean;
  error: boolean;
}

const initialState: BookingState = {
  bookings: [],
  reviews: [],
  loading: false,
  error: false,
};

export const BookingSlice = createSlice({
  name: "bookings",
  initialState,
  reducers: {
    GetMyBookingStart: (state) => {
      return { ...state, loading: true, error: false };
    },
    GetMyBookingError: (state) => {
      return { ...state, loading: false, error: true };
    },
    GetMyBookingSucess: (state, action: PayloadAction<Booking[]>) => {
      return {
        ...state,
        error: false,
        loading: false,
        bookings: action.payload,
      };
    },
    GetMyReviewStart: (state) => {
      return { ...state, loading: true, error: false };
    },
    GetMyReviewError: (state) => {
      return { ...state, loading: false, error: true };
    },
    GetMyReviewSucess: (state, action: PayloadAction<Review[]>) => {
      return {
        ...state,
        error: false,
        loading: false,
        reviews: action.payload,
      };
    },
    
  },
});

export const {
  GetMyBookingStart,
  GetMyBookingError,
  GetMyBookingSucess,
  GetMyReviewStart,
  GetMyReviewError,
  GetMyReviewSucess,
} = BookingSlice.actions;
export default BookingSlice.reducer;
