import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Review } from "../models/ReviewModel";
import { User } from "../models/UserModel";
import { Tour } from "../models/TourModel";

interface ReviewState {
    tour: Tour | null;
    user: User | null;
    loading: boolean;
    error: boolean;
}

const initialState: ReviewState = {
    tour: null,
    user: null,
    loading: false,
    error: false
}

export const reviewSlice = createSlice({
    name: 'review',
    initialState,
    reducers: {
        PostReviewStart: (state) => {
            return {...state, loading: true, error: false};
        },
        PostReviewError: (state) => {
            return {...state, loading: false, error: true};
        },
        PostReviewSucess: (state, action:PayloadAction<Review>) => {
            return {...state, loading: false, error: false, reivew: action.payload}
        }
    }
})

export const {
    PostReviewStart,
    PostReviewError,
    PostReviewSucess
} = reviewSlice.actions;
export default reviewSlice.reducer;