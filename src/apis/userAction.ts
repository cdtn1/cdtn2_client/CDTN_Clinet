import {
  AnyAction,
  Dispatch,
  EmptyObject,
  ThunkDispatch,
} from "@reduxjs/toolkit";
import axios, { AxiosInstance } from "axios";
import {
  GetMyBookingStart,
  GetMyBookingError,
  GetMyBookingSucess,
  GetMyReviewStart,
  GetMyReviewError,
  GetMyReviewSucess,
} from "../redux/actionSlice";
import { SetAlert } from "../redux/alertSlice";

axios.defaults.withCredentials = true;
const link = process.env.REACT_APP_API_LINK;

export const ByTour = async (
  data: any,
  dispatch: ThunkDispatch<EmptyObject, undefined, AnyAction> &
    Dispatch<AnyAction>,
  axiosJWT: AxiosInstance,
  accessToken: string | undefined
) => {
  try {
    const res = await axiosJWT.post(`${link}/api/v1/bookings/user`, data, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(SetAlert(res.data));
  } catch (error) {
    // @ts-ignore
    dispatch(SetAlert(error?.response?.data));
  }
};

export const createReivew = async (
  data: any,
  dispatch: ThunkDispatch<EmptyObject, undefined, AnyAction> &
    Dispatch<AnyAction>,
  axiosJWT: AxiosInstance,
  accessToken: string | undefined
) => {
  try {
    const res = await axiosJWT.post(`${link}/api/v1/tours/${data.tourId}/reviews`, data, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(SetAlert(res.data));
  } catch (error) {
    // @ts-ignore
    dispatch(SetAlert(error?.response?.data));
  }
};

export const getMyBooking = async (
  dispatch: ThunkDispatch<EmptyObject, undefined, AnyAction> &
    Dispatch<AnyAction>,
  axiosJWT: AxiosInstance,
  accessToken: string | undefined
) => {
  dispatch(GetMyBookingStart());
  try {
    const res = await axiosJWT.get(`${link}/api/v1/bookings/myBooking`, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(GetMyBookingSucess(res.data?.bookings));
  } catch (error) {
    dispatch(GetMyBookingError());
  }
};
export const getMyReview = async (
  dispatch: ThunkDispatch<EmptyObject, undefined, AnyAction> &
    Dispatch<AnyAction>,
  axiosJWT: AxiosInstance,
  accessToken: string | undefined
) => {
  dispatch(GetMyReviewStart());
  try {
    const res = await axiosJWT.get(`${link}/api/v1/reviews/myReview`, {
      headers: { token: `Bearer ${accessToken}` },
    });
    dispatch(GetMyReviewSucess(res.data?.reviews));
  } catch (error) {
    dispatch(GetMyReviewError());
  }
};

