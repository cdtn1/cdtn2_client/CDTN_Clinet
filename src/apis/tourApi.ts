import {AnyAction, Dispatch, EmptyObject, ThunkDispatch} from "@reduxjs/toolkit";
import axios, { AxiosInstance } from "axios";
import {
  GetListTourStart,
  GetListTourError,
  GetListTourSucess,
  GetTourStart,
  GetTourError,
  GetTourSucess,
} from "../redux/tourSlice";
axios.defaults.withCredentials = true;
const link = process.env.REACT_APP_API_LINK;

export const getAllTour = async (dispatch: ThunkDispatch<EmptyObject , undefined, AnyAction> & Dispatch<AnyAction>) => {
  dispatch(GetListTourStart());
  try {
    const res = await axios.get(`${link}/api/v1/tours`);
    dispatch(GetListTourSucess(res.data?.data.doc));
  } catch (error) {
    dispatch(GetListTourError());
  }
};

export const getTour = async (
  dispatch: ThunkDispatch<EmptyObject , undefined, AnyAction> & Dispatch<AnyAction>,
  id: string | undefined
) => {
  dispatch(GetTourStart());
  try {
    const res = await axios.get(`${link}/api/v1/tours/${id}`);
    dispatch(GetTourSucess(res.data?.data.doc));
  } catch (error) {
    dispatch(GetTourError());
  }
};

export const getAllTourByAdmin = async (
  dispatch: ThunkDispatch<EmptyObject, undefined, AnyAction> &
    Dispatch<AnyAction>,
  axiosJWT: AxiosInstance,
  accessToken: string | undefined
) => {
  dispatch(GetListTourStart());
}