import Header from "./Header";
import Description from "./Description";
import Pictures from "./Pictures";
import Reviews from "./Reviews";
import Footer from "./Footer";
import MapBox from "./MapBox";
import CreateReview from "./CreateReview";
import ShowMap from "./ShowMap";
export {
  Header,
  Description,
  Pictures,
  Reviews,
  Footer,
  MapBox,
  CreateReview,
  ShowMap,
};
