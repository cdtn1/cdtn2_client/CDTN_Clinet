import React, { useState } from "react";
import Map, { Marker, MapLayerMouseEvent } from "react-map-gl";
interface map {
    type: string,
    coordinates: number[],
    address?: string,
    description?: string
}
const MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiaGllcGZrIiwiYSI6ImNreDc3djRhdTJtdGwycXB6enMxdjJ6azYifQ.beqa-xkjz-nU-tYPIJ-3Bg";


export default function ShowMap() {
    const [showMap, setShowMap] = useState(false);
    const [startLocation, setStartLocation] = useState<map | null>(null);

    const handleOpenMap = () => {
        setShowMap(true);
    };

    const handleMapClick = (event: MapLayerMouseEvent & {lngLat: {lng: number; lat: number}})  => {
    // Lấy tọa độ khi người dùng chọn điểm trên bản đồ
        const { lngLat } = event;
        setStartLocation((prevStartLocation) => ({
            ...prevStartLocation,
            type: "Point",
            coordinates: [lngLat.lng, lngLat.lat],
        }));
        setShowMap(false);
    };

    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        // Gửi dữ liệu form (bao gồm tọa độ startLocation) lên server để tạo tour mới
        // ...
    };

    return (
        <form onSubmit={handleSubmit}>
        <label>Start Location</label>
        <input
            type="text"
            value={startLocation ? startLocation.coordinates.join(", ") : ""}
            readOnly
        />
        <button type="button" onClick={handleOpenMap}>
            Choose on Map
        </button>

        {/* Hiển thị bản đồ khi người dùng nhấp vào nút "Choose on Map" */}
        {showMap && (
            <section className="section-show-map">
                <div className="map">
                    <Map
                        // Cấu hình bản đồ
                        onClick={handleMapClick}
                        mapboxAccessToken={MAPBOX_ACCESS_TOKEN}
                        mapStyle="mapbox://styles/mapbox/light-v11"
                        scrollZoom
                    >
                        {startLocation && (
                        <Marker
                            longitude={startLocation.coordinates[0]}
                            latitude={startLocation.coordinates[1]}
                        >
                            <div className="marker"></div>
                        </Marker>
                        )}
                    </Map>
                </div>
            </section>
        )}

        <button type="submit">Submit</button>
        </form>
    );
}