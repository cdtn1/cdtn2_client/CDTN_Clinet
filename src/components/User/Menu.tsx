import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useAppSelector } from "../../redux/store"
interface MenuType {
  id: number;
  title: string;
  url: string;
  icon: string;
  role: string;
}
export default function Menu() {
  const [active, setActive] = useState<number>(1);
  const role = useAppSelector((state) => state.auth.user?.role)
  const list: MenuType[] = [
    {
      id: 1,
      title: "Settings",
      url: "",
      icon: "settings",
      role: "user"
    },
    {
      id: 2,
      title: "My Bookings",
      url: "bookings",
      icon: "briefcase",
      role: "user"
    },
    {
      id: 3,
      title: "My Reviews",
      url: "reviews",
      icon: "star",
      role: "user"
    },
    {
      id: 4,
      title: "Manage Tours",
      url: "mangertour",
      icon: "map",
      role: "admin"
    },
    {
      id: 5,
      title: "Manage Users",
      url: "",
      icon: "users",
      role: "admin"
    },
    {
      id: 6,
      title: "Manage Reivews",
      url: "",
      icon: "star",
      role: "admin"
    },
    {
      id: 7,
      title: "Manage Bookings",
      url: "",
      icon: "briefcase",
      role: "admin"
    },
  ];
  const filteredList = list.filter((item) => {
    if (role === "user") {
      return item.id >= 1 && item.id <= 3;
    } else if (role === "admin") {
      return item.id >= 1 && item.id <= 7;
    }
    return false;
  });
  return (
    <div className="user-view__menu">
      <ul className="side-nav">
        <>
          {filteredList.map((item) => {
            return (
              <li
                className={active === item.id ? "side-nav--active" : ""}
                key={item.id}
                onClick={() => setActive(item.id)}
              >
                <Link to={`/me/${item.url}`}>
                  <svg>
                    <use xlinkHref={`/assets/icons.svg#icon-${item.icon}`}/>
                  </svg>
                  {item.title}
                </Link>
              </li>
            );
          })}
        </>
      </ul>
    </div>
  );
}
