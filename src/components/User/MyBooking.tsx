import React, { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/store";
import { getMyBooking } from "../../apis/userAction";
import { LoginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../apis/createInstance";
import Loading from "../Loading";
import TourCard from "../TourCard";

export default function MyBooking() {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);
  const { bookings, loading } = useAppSelector((state) => state.action);
  let axiosJWT = createAxios(user, dispatch, LoginSuccess);
  
  useEffect(() => {
    getMyBooking(dispatch, axiosJWT, user?.accessToken);
  }, [dispatch]);

  if (loading) {
    return <Loading />;
  }

  if (!bookings || bookings?.length === 0) {
    return (
      <div style={{ padding: "0rem 5rem" }}>
        <h2 className="heading-secondary ma-bt-md">You never Booking before</h2>
      </div>
    );
  }

  return (
    <div style={{ padding: "0rem 5rem" }}>
      <h2 className="heading-secondary ma-bt-md">Your Booking</h2>
      <div className="card-container">
        {bookings?.map((booking) => {
          return (
            <div key={booking._id}>
              <TourCard tour={booking?.tour} />
            </div>
          );
        })}
      </div>
    </div>
  );
}
