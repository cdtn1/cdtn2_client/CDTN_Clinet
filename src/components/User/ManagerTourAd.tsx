import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../redux/store';
import { LoginSuccess } from "../../redux/authSlice";
import { createAxios } from "../../apis/createInstance";
import { getAllTourByAdmin } from "../../apis/tourApi";
import Loading from "../Loading";

export default function ManagerTourAd() {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);
  const { bookings, loading } = useAppSelector((state) => state.action);
  let axiosJWT = createAxios(user, dispatch, LoginSuccess);

  useEffect(() => {
    getAllTourByAdmin(dispatch, axiosJWT, user?.accessToken)
  }, [dispatch])

  if(loading) {
      return <Loading />;
  }
  if(!bookings || bookings.length === 0) {
      return (
        <div style={{ padding: "0rem 5rem" }}>
            <h2 className="heading-secondary ma-bt-md">Something was wrong</h2>
        </div>
      );
  }
  return (
    <div className='admin__manage__tour' style={{ padding: "0rem 5rem" }}>
      <button>
        <svg className="cart__icon_plus_tour">
          <use xlinkHref="/assets/icons.svg#icon-plus" />
        </svg>
      </button>
      <div>abc</div>
    </div>
  )
}


