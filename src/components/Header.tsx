import React from "react";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../redux/store";
import { logOutUser } from "../apis/authApi";
export default function Header() {
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);
  
  return (
    <header className="header">
      <nav className="nav nav--tours">
        <Link to="/" className="nav__el">
          All Tours{" "}
        </Link>

        <form className="nav__search">
          <button className="nav__search-btn">
            <svg>
              <use xlinkHref="icons.svg#icon-search" />
            </svg>
          </button>
          <input type="text" placeholder="Search tours" className="nav__search-input" />
        </form>
      </nav>
      <div className="header__logo">
        <Link to="/">
          <img src="/assets/logo-white.png" alt="" />
        </Link>
      </div>
      <nav className="nav nav--user">
        {user ? (
          <>
            <Link
              to="/"
              className="nav__el nav__el--logout"
              onClick={() => logOutUser(dispatch)}
            >
              Logout
            </Link>
            <Link to="/me"> 
              <div className="header__info-user" style={{display: "flex", alignItems: "center", gap: "5px"}}>
                <img src={user.photo} alt="" className="nav__user-img" />
                <div style={{color: "white",textTransform: "uppercase"}}>{user.name} ({user.role})</div>
              </div>
            </Link>
          </>
        ) : (
          <>
            <Link to="/login" className="nav__el">
              Login
            </Link>
            <Link to="/signup" className="nav__el nav__el--cta">
              Sign up
            </Link>
          </>
        )}
      </nav>
    </header>
  );
}
